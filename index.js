const pull = require('pull-stream')
const paramap = require('pull-paramap')
const Server = require('./server')

const HOPS = 10

async function main () {
  const ssb = await Server({
    friends: { hops: HOPS },
    conn: { hops: HOPS },
    lan: { legacy: false } // disables legacy UDP announce (which doesn't respect caps.shs!)
  })

  runAnalysis(ssb)
}
main()

function runAnalysis (ssb) {
  const PUBLIC_FIELDS = new Set(['type', 'tangles', 'authors', 'tombstone', 'preferredName', 'avatarImage'])

  const problemFields = (m) => {
    const fields = Object.entries(m.value.content)
      .reduce((acc, [key, value]) => {
        if (PUBLIC_FIELDS.has(key)) return acc
        if (value && value.set === null) return acc

        acc[key] = value.set || value
        return acc
      }, {})

    return Object.keys(fields).length
      ? fields
      : null
  }

  const profileId = (m) => (
    m.value.content.tangles.profile.root ||
    m.key
  )

  let count = 0
  const profileBreachs = {
    // profileId: [breach]
  }
  // look for public profiles
  pull(
    ssb.query.read({
      query: [{
        $filter: {
          value: {
            content: {
              type: 'profile/person'
            }
          }
        }
      }],
      // live: true,
      old: true
    }),

    // public
    pull.filter(m => m.value && !m.value.content.recps),

    // map/filter
    pull.map(m => {
      const breach = problemFields(m)

      return breach
        ? { profileId: profileId(m), breach }
        : null // filtered in next step
    }),
    pull.filter(Boolean),

    // pick only breachs on "critical" info fields
    // pull.filter(data => data.breach.address || data.breach.phone || data.breach.location), // ??

    pull.drain(
      ({ profileId, breach }) => {
        if (!profileBreachs[profileId]) profileBreachs[profileId] = []

        profileBreachs[profileId].push(breach)
      },
      err => {
        if (err) throw err

        pull(
          pull.values(Object.entries(profileBreachs)),
          paramap(
            ([profileId, breachs], cb) => {
              ssb.profile.get(profileId, (err, profile) => {
                if (err) {
                  console.log('woops')
                  return cb(err)
                }

                cb(null, { profile, breachs })
              })
            },
            6 // max concurrent
          ),
          pull.drain(({ profile, breachs }) => {
            // console.log(++count, profile.states[0].preferredName, '/', profile.states[0].legalName)
            // return

            console.log('\n')

            console.log(++count)
            console.log('id:', profile.key)
            console.log('preferredName:', profile.states[0].preferredName)
            console.log('legalName:', profile.states[0].legalName)
            console.log('breachs:', JSON.stringify(breachs, null, 2))
          })
        )
      }
    )
  )
}
